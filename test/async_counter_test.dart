import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_mocktail_examples/async_counter.dart';
import 'package:riverpod_mocktail_examples/count_by_two_provider.dart';
import 'package:riverpod_mocktail_examples/state_notifier.dart';

// class MockAsyncIntsKeepAlive extends Mock implements AsyncIntsKeepAlive {} <-- dont do this!
class MockAsyncIntsKeepAlive extends AsyncNotifier<int> with Mock implements AsyncIntsKeepAlive {}

// class MockAsyncIntsAutoDispose extends Mock implements AsyncIntsAutoDispose {} <-- dont do this!
class MockAsyncIntsAutoDispose extends AutoDisposeAsyncNotifier<int> with Mock implements AsyncIntsAutoDispose {}


// class MockTestNotifier extends Mock implements TestNotifier {} <-- wrong!
class MockTestNotifier extends StateNotifier<int> with Mock implements TestNotifier {
  MockTestNotifier(super.state);
}

void main() {

  group('auto dispose', () {
    ///
    /// Can mock an async build funtion!
    ///
    testWidgets('initial state is 888', (WidgetTester tester) async {
      final mock = MockAsyncIntsAutoDispose();
      when(mock.build).thenReturn(888);

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            asyncIntsAutoDisposeProvider.overrideWith(() => mock),
          ],
          child: const MaterialApp(
            home: AsyncCounterAutoDispose(),
          ),
        ),
      );

      expect(find.text('888'), findsOneWidget);
    });

    ///
    /// Verify functions were called
    ///
    testWidgets('tapping + invokes increment', (WidgetTester tester) async {
      final mock = MockAsyncIntsAutoDispose();
      when(mock.increment).thenAnswer((_) async {});

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            asyncIntsAutoDisposeProvider.overrideWith(() => mock),
          ],
          child: const MaterialApp(
            home: AsyncCounterAutoDispose(),
          ),
        ),
      );

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      // verify(mock.increment).called(1);
      // or
      verify(() => mock.increment()).called(1);
    });
  });

  group('keep alive', () {
    ///
    /// Can mock an async build funtion!
    ///
    testWidgets('initial state is 888', (WidgetTester tester) async {
      final mock = MockAsyncIntsKeepAlive();
      when(mock.build).thenReturn(888);

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            asyncIntsKeepAliveProvider.overrideWith(() => mock),
          ],
          child: const MaterialApp(
            home: AsyncCounterKeepAlive(),
          ),
        ),
      );

      expect(find.text('888'), findsOneWidget);
    });

    ///
    /// Verify functions were called
    ///
    testWidgets('tapping + invokes increment', (WidgetTester tester) async {
      final mock = MockAsyncIntsKeepAlive();
      when(mock.increment).thenAnswer((_) async {});

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            asyncIntsKeepAliveProvider.overrideWith(() => mock),
          ],
          child: const MaterialApp(
            home: AsyncCounterKeepAlive(),
          ),
        ),
      );

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      // verify(mock.increment).called(1);
      // or
      verify(() => mock.increment()).called(1);
    });
  });

  group('state notifier', () {
    testWidgets('initial state is 888', (WidgetTester tester) async {
      final mock = MockTestNotifier(888);

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            testNotifierProvider.overrideWith((ref) => mock),
          ],
          child: const MaterialApp(
            home: StateNotifierWidget(),
          ),
        ),
      );

      expect(find.text('888'), findsOneWidget);
    });

    ///
    /// Verify functions were called
    ///
    testWidgets('tapping + invokes increment', (WidgetTester tester) async {
      final mock = MockTestNotifier(888);

      await tester.pumpWidget(
        ProviderScope(
          overrides: [
            testNotifierProvider.overrideWith((ref) => mock),
          ],
          child: const MaterialApp(
            home: StateNotifierWidget(),
          ),
        ),
      );

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      // verify(mock.increment).called(1);
      // or
      verify(() => mock.increment()).called(1);
    });
  });
}
