// We create a "provider", which will store a value (here "Hello world").
// By using a provider, this allows us to mock/override the value exposed.
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'count_by_two_provider.g.dart';

@Riverpod(keepAlive: true)
int helloWorld(HelloWorldRef ref) {
  return 1;
}

@riverpod
class AsyncIntsAutoDispose extends _$AsyncIntsAutoDispose {
  @override
  FutureOr<int> build() async {
    await Future.delayed(const Duration(seconds: 1));
    return 0;
  }

  Future<void> increment() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      await Future.delayed(const Duration(seconds: 1));
      return (state.value ?? 0) + 2;
    });
  }
}

@Riverpod(keepAlive: true)
class AsyncIntsKeepAlive extends _$AsyncIntsKeepAlive {
  @override
  FutureOr<int> build() async {
    await Future.delayed(const Duration(seconds: 1));
    return 0;
  }

  Future<void> increment() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      await Future.delayed(const Duration(seconds: 1));
      return (state.value ?? 0) + 2;
    });
  }
}
