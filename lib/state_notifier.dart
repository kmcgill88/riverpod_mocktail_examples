import 'package:hooks_riverpod/hooks_riverpod.dart';


final testNotifierProvider = StateNotifierProvider<TestNotifier, int>((ref) => TestNotifier());

class TestNotifier extends StateNotifier<int> {
  TestNotifier() : super(0);

  void increment() {
    state  = state + 2;
  }
}
