// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'count_by_two_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

// ignore_for_file: avoid_private_typedef_functions, non_constant_identifier_names, subtype_of_sealed_class, invalid_use_of_internal_member, unused_element, constant_identifier_names, unnecessary_raw_strings, library_private_types_in_public_api

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

String _$AsyncIntsAutoDisposeHash() =>
    r'403c2543166446b8332093d6ea419a84307a0d62';

/// See also [AsyncIntsAutoDispose].
final asyncIntsAutoDisposeProvider =
    AutoDisposeAsyncNotifierProvider<AsyncIntsAutoDispose, int>(
  AsyncIntsAutoDispose.new,
  name: r'asyncIntsAutoDisposeProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$AsyncIntsAutoDisposeHash,
);
typedef AsyncIntsAutoDisposeRef = AutoDisposeAsyncNotifierProviderRef<int>;

abstract class _$AsyncIntsAutoDispose extends AutoDisposeAsyncNotifier<int> {
  @override
  FutureOr<int> build();
}

String _$AsyncIntsKeepAliveHash() =>
    r'6cf21f36dacb67cbba7a7eae98530132ea822203';

/// See also [AsyncIntsKeepAlive].
final asyncIntsKeepAliveProvider =
    AsyncNotifierProvider<AsyncIntsKeepAlive, int>(
  AsyncIntsKeepAlive.new,
  name: r'asyncIntsKeepAliveProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$AsyncIntsKeepAliveHash,
);
typedef AsyncIntsKeepAliveRef = AsyncNotifierProviderRef<int>;

abstract class _$AsyncIntsKeepAlive extends AsyncNotifier<int> {
  @override
  FutureOr<int> build();
}

String _$helloWorldHash() => r'0a90d922d75ba84b2748ba0b5fac44a88f06c80c';

/// See also [helloWorld].
final helloWorldProvider = Provider<int>(
  helloWorld,
  name: r'helloWorldProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$helloWorldHash,
);
typedef HelloWorldRef = ProviderRef<int>;
