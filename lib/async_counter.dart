import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_mocktail_examples/count_by_two_provider.dart';
import 'package:riverpod_mocktail_examples/state_notifier.dart';

class AsyncCounterAutoDispose extends HookConsumerWidget {
  const AsyncCounterAutoDispose({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncCounter = ref.watch(asyncIntsAutoDisposeProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Async Int'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            asyncCounter.maybeMap(
              data: (value) => Text(
                value.value.toString(),
                style: Theme.of(context).textTheme.headline4,
              ),
              loading: (data) => const CircularProgressIndicator(),
              orElse: () => const Text('orElse'),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ref.read(asyncIntsAutoDisposeProvider.notifier).increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}





class AsyncCounterKeepAlive extends HookConsumerWidget {
  const AsyncCounterKeepAlive({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncCounter = ref.watch(asyncIntsKeepAliveProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Async Int'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            asyncCounter.maybeMap(
              data: (value) => Text(
                value.value.toString(),
                style: Theme.of(context).textTheme.headline4,
              ),
              loading: (data) => const CircularProgressIndicator(),
              orElse: () => const Text('orElse'),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ref.read(asyncIntsKeepAliveProvider.notifier).increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}


class StateNotifierWidget extends HookConsumerWidget {
  const StateNotifierWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final testNotifier = ref.watch(testNotifierProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Async Int'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              testNotifier.toString(),
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ref.read(testNotifierProvider.notifier).increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
